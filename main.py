import os
# os.system("cls")
class Board():

	currentTurn = 'X'

	def __init__(self):
		self.cells = ["1" ,"2" , "3" , "4" , "5" , "6" , "7" , "8" , "9"]

	def display(self):
		print(" %s | %s | %s " %(self.cells[0] , self.cells[1] , self.cells[2]))
		print("-----------")
		print(" %s | %s | %s " %(self.cells[3] , self.cells[4] , self.cells[5]))
		print("-----------")
		print(" %s | %s | %s " %(self.cells[6] , self.cells[7] , self.cells[8]))

	def askStart(self):
		return input("would you like to play again ?! (Y / N)").upper()
		

	def startNew(self, board, choice):
		if choice.upper()== "Y":
			board.reset()
			return True
		else:
			return False

	def takeTurn(self, board):
		# if(self.currentTurn == 'O'):
			# do AI moves
			# return
		choice = int(input("\n'"+self.currentTurn+"_Choice'->> please choose a cell from 1 to 9!"))
		update = board.updateCell(choice , self.currentTurn)
		if(update):
			# update screen
			refreshScreen()
			# change turns
			# self.currentTurn = (self.currentTurn == 'X' ? 'O' : 'X')
			self.currentTurn = 'O' if self.currentTurn == 'X' else 'X'

		else:
			print 'Invalid move'





	def updateCell(self , cellNumber , player):
		if self.cells[cellNumber-1] != "X" and self.cells[cellNumber-1] != "O":
				self.cells[cellNumber-1]=player

				# Check if player (self.currentTurn) won
				if board.isWinner(board.currentTurn):
					print "\n "+board.currentTurn+" Wins!" 
					board.startNew(board, board.askStart())
					return True
				return True
		return False

	def isWinner(self , player):
		# Rows
		if self.cells[0]==player and self.cells[1]==player and self.cells[2]==player:
			return True
		if self.cells[3]==player and self.cells[4]==player and self.cells[5]==player:
			return True
		if self.cells[6]==player and self.cells[7]==player and self.cells[8]==player:
			return True
		# Columns
		if self.cells[0]==player and self.cells[3]==player and self.cells[6]==player:
			return True
		if self.cells[1]==player and self.cells[4]==player and self.cells[7]==player:
			return True
		if self.cells[2]==player and self.cells[5]==player and self.cells[8]==player:
			return True
		# Diagonal 1 (/)
		if self.cells[2]==player and self.cells[4]==player and self.cells[6]==player:
			return True
		# Diagonal 2 (\)
		if self.cells[0]==player and self.cells[4]==player and self.cells[8]==player:
			return True
		return False

	def isGameOver(self):
		usedCells = 0
		for cell in self.cells:
			if cell == "X" or cell == "O":
				usedCells += 1
		if usedCells == 9 :
			print usedCells
			return True
		else:
			return False

	def reset(self):
		self.cells = ["1" ,"2" , "3" , "4" , "5" , "6" , "7" , "8" , "9"]


board = Board()

def printHeader():
	print "\nlet's play ;D \n"

def refreshScreen():
	# os.system("cls")
	printHeader()
	board.display()

while True:

	board.takeTurn(board)
	
	if board.isGameOver():
			print "\n Game Over!"
			askStart = board.askStart()
			boardChoice =  board.startNew(board, askStart)
			if boardChoice:
					board.reset()
					continue
			else:
				   break
